<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");
require_once("model/blog.inc");

$blog = new Blog();
$blog->delete_unused_tags();
echo "Done.";
