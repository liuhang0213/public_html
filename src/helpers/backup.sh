#!/bin/bash
# Constants
BACKUP_DAILY_COPIES=3
BACKUP_WEEkLY_COPIES=3
# Day of the week to do weekly backups
WEEKLY_BACKUP_DAY=1

usage() {
    echo "Usage: backup.sh [-n DATABASE] [-u USERNAME] [-p PASSWORD] [-d DIR] [-b MYSQLDUMP]"
    exit 1
}

if (( $# < 4 )); then
    usage
fi

MYSQLDUMP_BIN=mysqldump

while getopts ":n:u:p:d:b:" opt; do
    case $opt in
        n)
            DATABASE=${OPTARG} ;;
        u)
            USERNAME=${OPTARG} ;;
        p)
            PASSWORD=${OPTARG} ;;
        d)
            BACKUP_DIR=${OPTARG} ;;
        b)
            MYSQLDUMP_BIN=${OPTARG} ;;
        *)
            usage ;;
    esac
done

BACKUP_DIR_DAILY="$BACKUP_DIR/daily"
BACKUP_DIR_WEEKLY="$BACKUP_DIR/weekly"
mkdir $BACKUP_DIR_DAILY 2> /dev/null
mkdir $BACKUP_DIR_WEEKLY 2> /dev/null

BACKUP_FILE_NAME="sql_backup_$(date -I)"

cd $BACKUP_DIR_DAILY
$MYSQLDUMP_BIN --user=php --password=phppass blog > $BACKUP_FILE_NAME
# Delete older copies, answer from
# https://stackoverflow.com/questions/25785/delete-all-but-the-most-recent-x-files-in-bash#25790
ls -tr | head -n -$BACKUP_DAILY_COPIES | xargs --no-run-if-empty rm

if (( $(date +%u) == $WEEKLY_BACKUP_DAY )); then
    cd $BACKUP_DIR_WEEKLY
    $MYSQLDUMP_BIN --user=php --password=phppass blog > $BACKUP_FILE_NAME
    ls -tr | head -n -$BACKUP_WEEkLY_COPIES | xargs --no-run-if-empty rm
fi
