CREATE TABLE users (
    user_id int AUTO_INCREMENT PRIMARY KEY,
    username varchar(255) NOT NULL UNIQUE,
    passwd binary(60) NOT NULL,
    name varchar(255) NOT NULL,
    email varchar(255),
    bio varchar(255)
);

CREATE TABLE articles (
    article_id int AUTO_INCREMENT PRIMARY KEY,
    title varchar(255) NOT NULL,
    user_id int NOT NULL,
    date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    text mediumtext,
    img varchar(255),
    published boolean NOT NULL,
    FULLTEXT (title,text),
    constraint fk_user foreign key (user_id) references users (user_id)
);

CREATE TABLE tags (
    tag_id int AUTO_INCREMENT PRIMARY KEY,
    tag_name varchar(255) NOT NULL UNIQUE
);

CREATE TABLE article_tag (
    article_id int NOT NULL,
    tag_id int NOT NULL,
    constraint fk_article FOREIGN KEY (article_id) REFERENCES articles (article_id) ON DELETE CASCADE,
    CONSTRAINT fk_tag FOREIGN KEY (tag_id) REFERENCES tags (tag_id) ON DELETE CASCADE,
    UNIQUE KEY unique_index (article_id, tag_id)
);

INSERT INTO users (username, passwd, name, email, bio) VALUES  ("user1", "$2y$10$zlRwdLTl6yS93tJeJZdCVe4yZJe2G3G9m6Dq6B5wlafjbsqyqYc6m", "Alice", "user1@domain", "This is the first user.");

INSERT INTO users (username, passwd, name, email, bio) VALUES  ("user2", "$2y$10$YaUPtUMESiJzFtYCUJld7uXlky5LwV3pPfbChb/LmlY2AbNjUTMxi", "Bob", "user2@domain", "This is the second user.");

INSERT INTO users (username, passwd, name, email, bio) VALUES  ("liuhang", "$2y$10$oTjeZZHuxvV.xY2qvACh4u.m1kNigCHBf.wPxy0Djv.iR7.ZDJPGm", "Liu Hang", "liuhang0213@gmail.com", "This is the real user.");
