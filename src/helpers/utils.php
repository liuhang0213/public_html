<?php
require_once("config/config.php");

function is_logged_in() {
    return (isset($_SESSION['user']));
}

function generate_years($start) {
    foreach (range(date('Y'), $start) as $year) {
        echo '<li><a href="javascript:set_filter_year('.$year.');">'
            .$year.'</a></li>';
    }
}

function make_summary($max_length, $string) {
    $re_string = '';
    $temp = '';
    $words = explode(' ', $string);
    while (strlen($temp) < $max_length && count($words) > 0) {
        $re_string = $temp;
        $temp .= $words[0].' ';
        $words = array_slice($words, 1);
    }

    if (count($words) == 0) {
        return $temp;
    } else {
        return trim($re_string, ' ').'&hellip;';
    }
}

// Database
function new_db_conn() {
    try {
            $conn = new PDO('mysql:host='.DB_SERVERNAME.';dbname=blog',
                DB_USERNAME, DB_PASSWD);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    return $conn;
}
?>
