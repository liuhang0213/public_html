/*
function nav_button() {
	var nav_display = document.getElementById("navbar");
	nav_display.style.position = (nav_display.style.position == "fixed") ? "relative" : "fixed";
	var nav_button = document.getElementById("navbutton");
	nav_button.setAttribute("style", "margin-top:" + Value.toString() + "px");
}
*/

function set_filter_year(year) {
	if (year == 0) {
		$('#dropdown-year').html('Any year <span class="caret"></span>');
		$('#dropdowngrp-month').css('display', 'none');
	} else {
		$('#dropdown-year').html(year);
		$('#dropdowngrp-month').css('display', 'block');
	}
	$('#filter-year').val(year);
}

function set_filter_month(month) {
	var month_array = ['NIL', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	if (month == 0) {
		$('#dropdown-month').html('Any month <span class="caret"></span>');
	} else {
		$('#dropdown-month').html(month_array[month]);
	}
	$('#filter-month').val(month);
}
