function confirm_delete(id) {
    var confirm = window.confirm("Do you want to delete the post?");
    if (confirm) {
        window.location = "/controller/delete_article?article_id=" + id
    } else {}
}

function preview(e, id) {
    var preview_row = $(e).closest('tr').next();
    preview_row.css('display',
        (preview_row.css('display') == "none") ? 'table-row' : 'none'
    );
}
