function add_fixed_tag() {
    // "Solidify" the entered tag
    if (/\S/.test($('#tag_input').val()) && $('#tag_input').val()) {
        // Check if the tag input is empty, or consists of only spaces
        $('#tags_display').append("<span class=\"tag tag_fixed\" onclick=\"remove_tag(this)\">"
            +$('#tag_input').val().trim().replace(',', '')
            +"</span>");
            $('#tag_input').val('');
    } else {}
}

function remove_tag(element) {
    $(element).remove();
}

function submit_with_tags(published) {
    if (!$('#title').val()) {
        alert("Title cannot be empty.");
        return;
    } else {}
    var tags = [];
    $('.tag_fixed').each(function(id) {
        tags.push($(this).html());
    });
    $('#tags').val(JSON.stringify(tags));
    $('#publish').val(published);
    $('#post').submit();
}

$(document).ready(function(){
$('#tag_input').keyup(function(e) {
    if (e.keyCode === 188) {
        add_fixed_tag()
    } else {}
});
});
