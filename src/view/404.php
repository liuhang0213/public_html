<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = "404 Page not found";
$head_extra = ' <link rel="stylesheet" href="/static/css/404.css">';

include("view/common/head.php");
include("view/common/header.php");
require_once("view/common/elements.php");;

$return_page = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: '/index';
$return_message = isset($_SERVER['HTTP_REFERER']) ? 'Return to previous page'
                    : 'Return to homepage';

row(<<< EOD
  <div class="title">
    <h2 class="inline">Page not found:&nbsp;</h2>
    <a class="title-link inline" id="redir" href="$return_page"><h2>$return_message</h2></a>
  </div>
  <div class="banner" id="default-pic"></div>
  <script>
  window.setTimeout(function() {
      location.href = document.getElementById("redir").href;
  }, 2000);
  </script>
EOD
);

include("view/common/footer.php");
?>
