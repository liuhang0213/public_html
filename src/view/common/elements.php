<?php
require_once("helpers/utils.php");

function row($row_content) {
?>
  <div class="row">
   <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
   <div class="maincontent col-xs-12 col-md-8 col-lg-6">
<?php
  echo $row_content;
?>
   </div>
  </div>
<?php
}

function post_entry_row($post_array) {
    // Used on the blog index page
?>
  <div class="row">
   <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
   <div class="maincontent post-entry col-xs-12 col-md-8 col-lg-6">
      <div class="post-img">
<?php
    if (isset($post_array['img'])) {
        echo '<img src="/static/img/blog/'.$post_array['img'].'""/>';
    } else {
        echo '<img src="/static/img/blog/default.jpg"/>';
    }
?>
      </div>
      <div class="post-entry-text">
<?php
    echo '<a  class="post-title" href="'.$post_array['link'].'">
        <h3>'.make_summary(40, htmlspecialchars_decode($post_array['title'])).
        ' by '.$post_array['author'].'</h3>
    </a>';
?>
          <p class='text'>
<?php
    echo make_summary(200,
        strip_tags(htmlspecialchars_decode($post_array['text'])));
?>
          </p>
      </div>
   </div>
  </div>
  <?php
  }

// These functions return strings instead of echoing them directly.

function post_entry_list($post_array) {
    return <<<EOD
    <tr>
        <td>${post_array['title']}</td>
        <td>
            <span class="cell-date">${post_array['date']}</span>
            <span class="cell-action">
                <i class="fa fa-eye" aria-hidden="true"></i> &nbsp;
                <a href="/view/blog/edit_article?id=${post_array['article_id']}">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                &nbsp;
                <a href="#">
                    <i class="fa fa-trash" aria-hidden="true" onclick="confirm_delete(${post_array['article_id']})"></i>
                </a>
            </span>
        </td>
    </tr>
EOD
;
}
