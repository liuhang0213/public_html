<!-- Banner & Navbar -->

<body>
<a href="#top"></a>
 <div class="row">
  <div class="header-container col-xs-12">
    <h1 class="header-title"><a href="/index.php">Liu Hang's Personal Page</a></h1>
  </div>
 </div>

 <input type="checkbox" class="navbutton-checkbox" id="nav" checked/>
 <div class="row">
  <div class="navbar-nav col-xs-12" id="navbar">
   <table class="nav-center">
    <tr>
     <td><a href="/view/about.php">About</a></td>
     <td><a href="/view/projects/project_index.php">Projects</a></td>
     <td><a href="/view/blog/articles_index.php">Blog</a></td>
     <td><a href="/view/bookmarks.php">Links</a></td>
    </tr>
  </table>
 </div>
 <div class="navbutton">
   <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
 </div>
</div>
<div class='container'>
  <div class='row'>
    <div class='maincontent col-md-9 col-lg-8'>
