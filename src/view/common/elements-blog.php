<?php
require_once("helpers/utils.php");

function row($row_content) {
?>
  <div class="row">
   <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
   <div class="maincontent col-xs-12 col-md-10 col-lg-9">
<?php
  echo $row_content;
?>
   </div>
  </div>
<?php
}

function post_entry_row($post_array) {
    // Used on the blog index page
?>
  <div class="row">
   <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
   <div class="maincontent post-entry col-xs-12 col-md-10 col-lg-8"
        style="background-image:
<?php
    if (isset($post_array['img'])) {
        echo htmlspecialchars('url("'.$post_array['img'].'");');
    } else {
        echo htmlspecialchars('url("/static/img/blog/default.jpg");');
    }
?>
    ">
      <div class="post-entry-text">
<?php
    echo '<a  class="post-title" href="'.$post_array['link'].'">
        <h3>'.make_summary(40, htmlspecialchars_decode($post_array['title'])).
        ' by '.$post_array['author'].'</h3>
    </a>';
?>
          <p class='text'>
<?php
    echo make_summary(200,
        strip_tags(htmlspecialchars_decode($post_array['text'])));
?>
          </p>
      </div>
   </div>
  </div>
  <?php
  }

function generate_tags($tags_array) {
    foreach ($tags_array as $tag) {
        echo '<a href="/view/blog/articles_index?tag_id='.$tag['tag_id'].'">'
            .$tag['tag_name']
            .'('
            .$tag['count(tag_id)']
            .')</a>&emsp;';
    }
}

// These functions return strings instead of echoing them directly.

function post_entry_list($post_array) {
    $preview_text = make_summary(500, strip_tags(htmlspecialchars_decode($post_array['text'])));
    $draft_title = ($post_array['published']) ? '' : '[Draft]';
    $draft_class = ($post_array['published']) ? '' : 'draft';
    return <<<EOD
    <tr class='$draft_class'>
        <td>
            <a target="_blank" href="/view/blog/view_article?id=${post_array['article_id']}">
                $draft_title ${post_array['title']}
            </a>
        </td>
        <td>
            <span class="cell-date">${post_array['date']}</span>
            <span class="cell-action">
                <a title='Preview' href="javascript:;">
                    <i class="fa fa-eye" aria-hidden="true" onclick="preview(this, ${post_array['article_id']})"></i>
                </a>
                &nbsp;
                <a title='Edit' href="/view/blog/edit_article?id=${post_array['article_id']}">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                &nbsp;
                <a title='Delete' href="javascript:;">
                    <i class="fa fa-trash" aria-hidden="true" onclick="confirm_delete(${post_array['article_id']})"></i>
                </a>
            </span>
        </td>
    </tr>
    <tr class='preview-text'>
        <td colspan=2>$preview_text</td>
    </tr>
EOD
;
}
