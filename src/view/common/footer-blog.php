</div>
<div class='col-md-3 col-lg-2 sidebar'>
    <hr>
    <a href="/view/blog/login.php">
      <h3 class="inline"><?php echo $login_status ?><i class="fa fa-angle-double-right" aria-hidden="true"></i></h3>
    </a>

    <hr/>
    See articles from: <br/>
    <div class="btn-group" role="group" id='dropdowngrp-year'>
        <div class="btn-group" role="group">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-year" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Any year
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu_year">
                <li><a href="javascript:set_filter_year(0);">---</a></li>
                <?php generate_years(2016) ?>
            </ul>
        </div>

        <div class="btn-group" role="group" id='dropdowngrp-month'>
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Any month
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu_month">
                <li><a href="javascript:set_filter_month(0);">---</a></li>
                <li><a href="javascript:set_filter_month(1);">January</a></li>
                <li><a href="javascript:set_filter_month(2);">February</a></li>
                <li><a href="javascript:set_filter_month(3);">March</a></li>
                <li><a href="javascript:set_filter_month(4);">April</a></li>
                <li><a href="javascript:set_filter_month(5);">May</a></li>
                <li><a href="javascript:set_filter_month(6);">June</a></li>
                <li><a href="javascript:set_filter_month(7);">July</a></li>
                <li><a href="javascript:set_filter_month(8);">August</a></li>
                <li><a href="javascript:set_filter_month(9);">September</a></li>
                <li><a href="javascript:set_filter_month(10);">October</a></li>
                <li><a href="javascript:set_filter_month(11);">November</a></li>
                <li><a href="javascript:set_filter_month(12);">December</a></li>
            </ul>
        </div>
        <button form='date-filter' type="submit" class="btn btn-default">Go</button>
        <form id='date-filter' action='/view/blog/articles_index' method='GET'>
            <input type='hidden' name='filter-year' id='filter-year' value=0>
            <input type='hidden' name='filter-month' id='filter-month' value=0>
            <input type="submit" id="submit-form" class="hidden" />
        </form>
    </div>
    <hr/>
    <form action='/view/blog/articles_index'>
        <div class='input-group'>
            <input class='form-control' id="searchbar" type='search' name='search' placeholder='Search for...'/>
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <i class="fa fa-search" aria-hidden="true"></i>                </button>
            </span>
        </div>
    </form>
    <hr/>
    Tags:<br/>
    <ul class="list-group">
        <?php generate_tags($blog->get_tag_counts()); ?>
    </ul>
</div>
</div>
</div>
<div class="row">
 <div id="footer" class="col-xs-12">
 Copyright © <?php echo date('Y'); ?> Liu Hang. All rights reserved.
 </div>
</div>

</body>
</html>
