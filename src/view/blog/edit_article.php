<?php
/* This page is both for creating a new post and updating an existing one, depending on
*    whether the article_id can be found in $_GET.
*/
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");
require_once("model/blog.inc");

session_start();
if (!is_logged_in()) {
    header('Location: '.'/view/blog/login.php?status=2&redir=/view/blog/user_article_list.php');
    exit();
} else {
    $user = $_SESSION['user'];
}

$blog = new Blog();
if (isset($_GET['id'])) {
    $article = $blog->get_article_by_id($_GET['id']);
    if ($article['user_id'] != $user->get_user_id()) {
        // Check whether the user is same as the author
        header('Location: '.'/view/404.php');
        exit();
    }

    $article_title = htmlspecialchars_decode($article['title']);
    $title = 'Edit post: '.$article_title;
    $text = htmlspecialchars_decode($article['text']);
    $new = false;
    $_SESSION['article_id'] = $article['article_id'];
    $tags = '<span class="tag_fixed" onclick="remove_tag(this)">'.
        implode(<<<EOD
        </span>
        <span class="tag_fixed" onclick="remove_tag(this)">
EOD
        , $blog->get_tags_by_article_id($article['article_id']))
        .'</span>';

} else {
    $article_title = '';
    $title = 'New post';
    $text = '';
    $new = true;
    $tags = '';
}

$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css"/>
<link rel="stylesheet" href="/static/css/edit_article.css"/>
<script src="/static/tinymce/js/tinymce/tinymce.min.js"></script>
<script>tinymce.init({
    selector:'textarea',
    entity_encoding : 'raw',
    theme: 'modern',
    plugins: [
        'autolink lists link image charmap preview hr anchor',
        'searchreplace visualblocks visualchars code',
        'media table directionality',
        'paste textcolor colorpicker imagetools codesample'
    ],
    toolbar1: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image preview media | forecolor backcolor | codesample',
    image_advtab: true
});</script>
<script src="/static/js/edit_article.js"></script>
EOD;

include("view/common/head.php");
include("view/common/header.php");
require_once("view/common/elements.php");

row(<<< EOD
    <div class="title"><h2>$title</h2></div>
    <div>
        <form id="post" action="/controller/update_article.php?new=$new" method="post">
            <input id='title' type="text" name="title" placeholder="Title" value="${article_title}" required/><br/>
            <br/>
            <textarea id='editor' class="main-text-input" name="text" rows="10">$text</textarea><br/>
            Tags:
            <span id="tags_display">
                $tags
            </span>
            <input id="tag_input" type="text" placeholder="" onblur="add_fixed_tag()"/><br/>
            <input id="tags" type="hidden" name="tags" value=""/>
            <input id="publish" type='hidden' name="publish" value=""/>
            <br/>
            <div class="center">
                <button class="btn btn-primary" type="button" onclick="submit_with_tags(1)">
                    Publish
                </button>
                <button class="btn btn-default" type="button" onclick="submit_with_tags(0)">
                    Save as draft
                </button>
            </div>
        </form>
    </div>
EOD
);

include("view/common/footer.php");
