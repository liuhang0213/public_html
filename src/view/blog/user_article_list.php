<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");
require_once("model/blog.inc");

session_start();
if (!is_logged_in()) {
    header('Location: '.'/view/blog/login.php?status=2&redir=/view/blog/user_profile.php');
} else {
    $login_status = 'My Dashboard';
}

$user = $_SESSION['user'];
$blog = new Blog();
$title =  $user->name.'\'s Posts';
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css">
<link rel="stylesheet" href="/static/css/user_article_list.css">
<script src="/static/js/user_article_list.js"></script>
EOD;

include("view/common/head.php");
include("view/common/header-blog.php");
require_once("view/common/elements-blog.php");

// Gravatar
$userMail = $user->email;
$imageWidth = '120'; //The image size
$imgUrl = 'http://www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;

$pred = function($article) {
    return ($article['user_id'] == $user->get_user_id());
};

$blog->filter_articles_by_user_id($user->get_user_id());

$array = $blog->post_loader(0, 10);
$posts_list = "";

foreach ($array as $post_array) {
    if (!is_null($post_array)) {
        $posts_list .= post_entry_list($post_array);
    }
}

row(<<< EOD
    <div class="title">
        <img class="profile-pic" src="$imgUrl" />&emsp;
        <h2 class="inline">$user->name</h2>
    </div>
    <table class="table post-list">
    <tr>
        <th>Title</th><th>Date<th>
    </tr>
    $posts_list
    </table>
EOD
);

include("view/common/footer-blog.php");
