<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("config/config.php");
require_once("helpers/utils.php");
require_once("model/blog.inc");
require_once("model/user.inc");


$blog = new Blog();
$blog->filter_published_articles();
$filter = '';
$filtered_message = '';

if (isset($_GET['tag_id'])) {
    $blog->filter_articles_by_tag_id($_GET['tag_id']);
    $filter .= '&tag_id='.$_GET['tag_id'];
    $filtered_message = 'Showing articles tagged "'.$blog->get_tag_name($_GET['tag_id']).'"';
}

if (isset($_GET['author_id'])) {
    $author = new User('user_id', $_GET['author_id']);
    $blog->filter_articles_by_user_id($_GET['author_id']);
    $filter .= '&author_id='.$_GET['author_id'];
    $filtered_message = 'Showing articles by '.$author->name;
}

if (isset($_GET['search'])) {
    $blog->search_articles(htmlspecialchars($_GET['search']));
    $blog->filter_published_articles();
    $filter .= '&search='.$_GET['search'];
    $filtered_message = 'Showing articles containing "'.$_GET['search'].'"';
}

if (isset($_GET['filter-year']) && $_GET['filter-year'] != 0) {
    $year = $_GET['filter-year'];
    $month = $_GET['filter-month'];
    if ($month != 0) {
        $filtered_message = 'Showing articles from '.MONTH[$month].' '.$year;
    } else {
        $filtered_message = 'Showing articles from '.$year;
    }
    $blog->filter_articles_by_date($month, $year);
    $blog->filter_published_articles();
}


$start = isset($_GET['start']) ? (int)$_GET['start'] : 0;

$prev = $start - POSTS_PER_PAGE;
$next = $start + POSTS_PER_PAGE;

// Navigating through posts

$get_prev = $filter.'&start='.$prev;
$get_next = $filter.'&start='.$next;
$prev_display = 'default';
$next_display = 'default';

if (0 > $prev && $prev > (0 - POSTS_PER_PAGE)) {
    // Less than POSTS_PER_PAGE in the previous page; shouldn't happen.
    $get_prev = $filter;
}

if ($next >= $blog->count_selected_posts()) {
    // Last page
    $next_display = 'none';
}

if ($prev == (0 - POSTS_PER_PAGE)){
    // First page
    $prev_display = 'none';
}

if ($prev < 0 - POSTS_PER_PAGE || $next > POSTS_PER_PAGE + $blog->count_selected_posts()) {
    // Outside range; should not happen
    header('Location: view/404.php?redir=/view/blog/articles_index');
}

session_start();
$login_status = (is_logged_in()) ? 'My Dashboard ' : 'Login ';

$title = 'Liu Hang - Blog';
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css">
<link rel="stylesheet" href="/static/css/articles_index.css">
EOD;

include("view/common/head.php");
include("view/common/header-blog.php");
require_once("view/common/elements-blog.php");

row(<<< EOD
  <div class="title"><h2 class="inline">Blog</h2>
  </div>
  <div>
    $filtered_message
  </div>
EOD
);

$array = $blog->post_loader($start, POSTS_PER_PAGE);

foreach ($array as $article_array) {
    if (!is_null($article_array)) {
        post_entry_row($article_array);
    }
}

row(<<< EOD
<a class="page-turn" style="display: $prev_display;" href="/view/blog/articles_index?$get_prev">
  <h3 class="inline"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous Page</h3>
</a>
<a class="page-turn right-link" style="display: $next_display;" href="/view/blog/articles_index?$get_next">
  <h3 class="inline">Next Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></h3>
</a>
EOD
);

include("view/common/footer-blog.php");
