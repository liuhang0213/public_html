<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");
require_once("model/blog.inc");

session_start();
if (!isset($_GET['id'])) {
    header('Location: view/404.php');
} else {
    $id = $_GET['id'];
}

$login_status = (is_logged_in()) ? 'My Dashboard ' : 'Login ';
//$user = $_SESSION['user'];
$blog = new Blog();

$article = $blog->get_article_by_id($id);
$title = $article['author']." - ".$article['title'];
$article_title = htmlspecialchars_decode($article['title']);
$article_text = htmlspecialchars_decode($article['text']);

$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css">
EOD;

include("view/common/head.php");
include("view/common/header-blog.php");
require_once("view/common/elements-blog.php");

row(<<< EOD
  <div class="post-title"><h2>${article_title}</h2></div>
  <div class="sub-text">by
    <a class='sub-text' href="/view/blog/user_profile?user_id=${article['author_id']}">${article['author']}</a>
    , published on ${article['date']}</div>
EOD
);

row(<<< EOD
  <div class="main-text col-xs-12">
    <p>
    ${article_text}
    </p>
  </div>
EOD
);

$tags = $blog->get_tags_by_article_id($article['article_id']);
foreach ($tags as &$tag) {
    $tag = '<a class="sub-text" href="/view/blog/articles_index?tag_id='.$blog->get_tag_id($tag).
                '">'.$tag.'</a>';
}

$tags = implode(', ', $tags);
row(<<< EOD
    <div class='sub-text'>
        Tags: $tags
    </div>
EOD
);

$prev = $blog->get_previous_article($article['article_id']);
if (!$prev) {
    $prev_display = 'none';
    $prev_id = 0;
    $prev_title = '';
} else {
    $prev_display = 'inline-block';
    $prev_id = $prev['article_id'];
    $prev_title = make_summary(100, $prev['title']);
}

$next = $blog->get_next_article($article['article_id']);
if (!$next) {
    $next_display = 'none';
    $next_id = 0;
    $next_title = '';
} else {
    $next_display = 'inline-block';
    $next_id = $next['article_id'];
    $next_title = make_summary(100, $next['title']);
}

row(<<< EOD
<a class="page-turn" style="display: $prev_display;" href="/view/blog/view_article?id=$prev_id">
  <h3 class="inline"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous Article: <br/>$prev_title</h3>
</a>
<a class="page-turn right-link" style="display: $next_display;" href="/view/blog/view_article?id=$next_id">
  <h3 class="inline">Next Article: <i class="fa fa-angle-double-right" aria-hidden="true"></i><br/>$next_title</h3>
</a>
EOD
);

include("view/common/footer-blog.php");
