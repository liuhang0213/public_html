<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");

session_start();

if (is_logged_in()) {
    header('Location: '.'/view/blog/user_dashboard.php');
    exit;
}

$title = "Login";
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/login.css">
EOD;

$status = isset($_GET["status"]) ? $_GET["status"] : 0;
$username = isset($_GET["username"]) ? $_GET["username"] : "";
$redirect_from = isset($_GET["redir"]) ? $_GET["redir"] : "";

include("view/common/head.php");
include("view/common/header.php");
require_once("view/common/elements.php");


switch($status) {
    case 1: // Wrong credentials
        $message = '<div class="alert alert-danger" role="alert">Invalid username or password.</div>';
        break;
    case 2: // Redirected
        $_SESSION["redirect_from"] = $redirect_from;
        $message = '<div class="alert alert-warning" role="alert">Please login first.</div>';
        break;
    default:
        $message = '';
        break;
}

row(<<< EOD
<div class="title"><h2>Login</h2></div>
<form action="/controller/login_action.php" method="post">
$message
<p><input type="text" name="username" placeholder="Username" value="$username"/></p>
<p><input type="password" name="passwd" placeholder="Password"/></p>
<p><input class='btn btn-lg btn-default' type="submit" value="Login"/></p>
</form>
EOD
);
include("view/common/footer.php");
