<?php
// This page should be made available to public, but only author can edit the details

set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");
require_once("model/blog.inc");

session_start();
$blog = new Blog();
/*
if (!is_logged_in()) {
    header('Location: /view/blog/login.php?status=2&redir=/view/blog/user_profile.php');
} else {
    $user = $_SESSION['user'];
}
*/

$login_status = (is_logged_in()) ? 'My Dashboard ' : 'Login ';
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : -1;
$viewed_user = new User("user_id", $user_id);
if (is_null($viewed_user->name)) {
    header('Location: /view/404.php');
    exit();
}

$title =  $viewed_user->name.'\'s Profile';
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css">
EOD;

include("view/common/head.php");
include("view/common/header-blog.php");
require_once("view/common/elements-blog.php");

// Gravatar
$userMail = $viewed_user->email;
$imageWidth = '120'; //The image size
$imgUrl = 'http://www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;
row(<<< EOD
    <div class="title">
        <img class="profile-pic" src="$imgUrl" /> &emsp;
        <h2 class="inline">$viewed_user->name</h2>
    </div>
EOD
);
row(<<< EOD
    <div class="main-text col-xs-12">
        <p>
        Email: $viewed_user->email <br/>
        About me: $viewed_user->bio <br/>
        <a href="/view/blog/articles_index?author_id=$user_id">
            See all posts by $viewed_user->name
        </a>
        </p>
    </div>
EOD
);

include("view/common/footer-blog.php");
