<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");

session_start();
if (!is_logged_in()) {
    header('Location: '.'/view/blog/login.php?status=2');
    exit();
} else {
    $user = $_SESSION['user'];
}

$user_id = $user->get_user_id();
$title = $user->name.'\'s Dashboard';
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/blog.css">
EOD;

include("view/common/head.php");
include("view/common/header.php");
require("view/common/elements.php");

// Gravatar
$userMail = $user->email;
$imageWidth = '120'; //The image size
$imgUrl = 'http://www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;

row(<<< EOD
    <div class="title">
        <img class="profile-pic" src="$imgUrl" />&emsp;
        <h2 class="inline">$user->name&#39;s Dashboard</h2>
        <a class="title-link right-link" href="/controller/logout_action.php">
            <h2 class="inline">Logout <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2>
        </a>
    </div>
    <div class="col-xs-12">
    <ul class="list-group">
        <li class="list-group-item"><a href="/view/blog/user_profile?user_id=${user_id}">My Profile</a></li>
        <li class="list-group-item"><a href="/view/blog/user_article_list">My Posts</a></li>
        <li class="list-group-item"><a href="/view//blog/edit_article">New Post</a></li>
        </ul>
        </div>
EOD
);

include("view/common/footer.php");
