<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = "Liu Hang - Projects";
$head_extra = <<<EOD
 <link rel="stylesheet" href="/static/css/projects.css">
EOD;

include("view/common/head.php");
include("view/common/header.php");
?>

<div class="row">
 <div class="filler col-xs-0 col-sm-1 col-md-2"></div>
 <div class="maincontent col-xs-12 col-sm-10 col-md-8">
  <div class="title"><h2>My projects</h2> </div>

  <div class="project-tile col-xs-12 col-md-6 col-lg-4">
    <div class="project-pic">
      <img src="/static/img/projects.jpg"></img>
    </div>
    <a href="./project_schoenberg.php">
     <div class="project-title">
     </div>
     <h3>Arnold Schoenberg Playing Cards</h3>
    </a>
  </div>
</div>
</div>


<?php
include("view/common/footer.php");
?>
