<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = "Default Page";
$head_extra = '<link rel="stylesheet" href="./project_schoenberg.css">';
include("view/common/head.php");
include("view/common/header.php");
?>

<div class="row">
 <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
 <div class="maincontent col-xs-12 col-md-8 col-lg-6">
  <div class="title"><h2>Schoenberg Playing Cards</h2></div>
  <div class="banner" id="schoenberg">
  </div>
  <div class="main-text col-xs-12">
  <p>I made some cards from Arnold Schoenberg's designs.  Schoenberg was a composer
     known for his atonal works.  He had other interests such as painting and designing.
     Below is the link for my redesign of the card set:
  </p>
  <p>
    <a href="/static/resources/Schoenberg_cards.pdf"><kbd>Schoenberg_cards.pdf</kbd></a>
  </p>
 </div>
</div>

<?php
include("view/common/footer.php");
?>
