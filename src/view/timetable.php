<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = 'Liu Hang - My NUSMods.com Timetable';
$head_extra = '
<link rel="stylesheet" href="/static/css/timetable.css">
';
include("view/common/head.php");
include("view/common/header.php");
require_once("view/common/elements.php");;
?>
<div class="row">
    <div class="col-xs-0 col-md-1"></div>
<div class="col-xs-12 col-md-10">
<table class="table table-bordered table-condensed">
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<colgroup span="2"></colgroup>
<tbody>
<tr id="times">
  <th><div>0800</div></th>
  <th colspan="2"><div>0900</div></th>
  <th colspan="2"><div>1000</div></th>
  <th colspan="2"><div>1100</div></th>
  <th colspan="2"><div>1200</div></th>
  <th colspan="2"><div>1300</div></th>
  <th colspan="2"><div>1400</div></th>
  <th colspan="2"><div>1500</div></th>
  <th colspan="2"><div>1600</div></th>
  <th colspan="2"><div>1700</div></th>
  <th colspan="2"><div>1800</div></th>

</tr>
</tbody>
<tbody class="day" id="mon">
<tr>
  <th rowspan="2"><div>M<br>O<br>N</div></th>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00 current-day-time"></td>
  <td class="h11 m30 current-day-time"></td>
  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00" colspan="2">
    <div class="lesson color7 ui-draggable ui-draggable-handle" data-hasqtip="12">
        CS2100 TUT <span class="group">[2]</span>
        <div class="room">COM1-0210</div>
    </div>
  </td>

  <td class="h14 m00" colspan="2"><div class="lesson color7 ui-draggable ui-draggable-handle" data-hasqtip="13"><div>
  <span class="code">CS2100</span>
  <span class="title">Computer Organisation</span>
</div>
LAB <span class="group">[6]</span>
<div class="room">COM1-0114</div>
<div class="week"></div>
</div></td>

  <td class="h15 m00"></td>
  <td class="h15 m30"></td>
  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>
</tr>
<tr>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00 current-day-time"></td>
  <td class="h11 m30 current-day-time"></td>
  <td class="h12 m00"></td><td class="h12 m30"></td><td class="h13 m00"></td><td class="h13 m30"></td>



  <td class="h14 m00"></td>
  <td class="h14 m30"></td>
  <td class="h15 m00"></td>
  <td class="h15 m30"></td>
  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>
</tr>
</tbody>
<tbody class="day" id="tue">
<tr>
  <th rowspan="2"><div>T<br>U<br>E</div></th>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00" colspan="2"><div class="lesson color2 ui-draggable ui-draggable-handle" data-hasqtip="20"><div>
  <span class="code">GET1029</span>
  <span class="title">Life, the Universe, and Everything</span>
</div>
TUT <span class="group">[W9]</span>
<div class="room">AS2-0311</div>
<div class="week"></div>
</div></td>

  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00"></td>
  <td class="h11 m30"></td>
  <td class="h12 m00" colspan="4"><div class="lesson color4" data-hasqtip="6"><div>
  <span class="code">CS2020</span>
  <span class="title">Data Structures and Algorithms Accelerated</span>
</div>
LEC <span class="group">[1]</span>
<div class="room">LT15</div>
<div class="week"></div>
</div></td>



  <td class="h14 m00"></td>
  <td class="h14 m30"></td>
  <td class="h15 m00" colspan="6"><div class="lesson color5 ui-draggable ui-draggable-handle" data-hasqtip="14"><div>
  <span class="code">IS1103</span>
  <span class="title">Computing and Society</span>
</div>
SEC <span class="group">[4]</span>
<div class="room">i3-0344</div>
<div class="week"></div>
</div></td>
</tr>
<tr>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00"></td>
  <td class="h11 m30"></td>
  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00"></td>
  <td class="h13 m30"></td>
  <td class="h14 m00"></td><td class="h14 m30"></td><td class="h15 m00"></td><td class="h15 m30"></td>



  <td class="h16 m00"></td><td class="h16 m30"></td><td class="h17 m00"></td><td class="h17 m30"></td>

</tr>
</tbody>
<tbody class="day" id="wed">
<tr>
  <th rowspan="2"><div>W<br>E<br>D</div></th>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00" colspan="4"><div class="lesson color2" data-hasqtip="19"><div>
  <span class="code">GET1029</span>
  <span class="title">Life, the Universe, and Everything</span>
</div>
LEC <span class="group">[1]</span>
<div class="room">LT11</div>
<div class="week"></div>
</div></td>



  <td class="h12 m00" colspan="4"><div class="lesson color7" data-hasqtip="10"><div>
  <span class="code">CS2100</span>
  <span class="title">Computer Organisation</span>
</div>
LEC <span class="group">[1]</span>
<div class="room">i3-Aud</div>
<div class="week"></div>
</div></td>



  <td class="h14 m00"></td>
  <td class="h14 m30"></td>
  <td class="h15 m00"></td>
  <td class="h15 m30" colspan="3"><div class="lesson color6 ui-draggable ui-draggable-handle" data-hasqtip="15"><div>
  <span class="code">WR1401</span>
  <span class="title">Workplace Readiness</span>
</div>
SEC <span class="group">[G11]</span>
<div class="room">RVR-UTSCWL</div>
<div class="week"></div>
</div></td>


  <td class="h17 m00"></td>
  <td class="h17 m30"></td>

</tr>
<tr>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td><td class="h10 m30"></td><td class="h11 m00"></td><td class="h11 m30"></td>



  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00"></td>
  <td class="h13 m30"></td>
  <td class="h14 m00"></td><td class="h14 m30"></td><td class="h15 m00"></td><td class="h15 m30"></td>



  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>
</tr>
</tbody>
<tbody class="day" id="thu">
<tr>
  <th rowspan="2"><div>T<br>H<br>U</div></th>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td><td class="h10 m30"></td><td class="h11 m00"></td><td class="h11 m30"></td>



  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00" colspan="4"><div class="lesson color0 ui-draggable ui-draggable-handle" data-hasqtip="18"><div>
  <span class="code">GEQ1917</span>
  <span class="title">Understanding and Critiquing Sustainability</span>
</div>
SEC <span class="group">[G17]</span>
<div class="room">RVR-SEMRMB</div>
<div class="week"></div>
</div></td>



  <td class="h15 m00" colspan="2"><div class="lesson color7" data-hasqtip="11"><div>
  <span class="code">CS2100</span>
  <span class="title">Computer Organisation</span>
</div>
LEC <span class="group">[1]</span>
<div class="room">i3-Aud</div>
<div class="week"></div>
</div></td>

  <td class="h16 m00" colspan="4"><div class="lesson color4 ui-draggable ui-draggable-handle" data-hasqtip="28"><div>
  <span class="code">CS2020</span>
  <span class="title">Data Structures and Algorithms Accelerated</span>
</div>
TUT <span class="group">[8]</span>
<div class="room">COM1-0201</div>
<div class="week"></div>
</div></td>

</tr>
<tr>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00"></td>
  <td class="h11 m30"></td>
  <td class="h12 m00"></td><td class="h12 m30"></td><td class="h13 m00"></td><td class="h13 m30"></td>



  <td class="h14 m00"></td>
  <td class="h14 m30"></td>
  <td class="h15 m00"></td>
  <td class="h15 m30"></td>
  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>
</tr>
</tbody>
<tbody class="day" id="fri">
<tr>
  <th rowspan="2"><div>F<br>R<br>I</div></th>
  <td class="h08 m00" colspan="4"><div class="lesson color1 ui-draggable ui-draggable-handle" data-hasqtip="16"><div>
  <span class="code">ES1601</span>
  <span class="title">Professional and Academic Communication</span>
</div>
TUT <span class="group">[G17]</span>
<div class="room">RVR-SEMRMB</div>
<div class="week"></div>
</div></td>



  <td class="h10 m00" colspan="4"><div class="lesson color4" data-hasqtip="7"><div>
  <span class="code">CS2020</span>
  <span class="title">Data Structures and Algorithms Accelerated</span>
</div>
LEC <span class="group">[1]</span>
<div class="room">LT15</div>
<div class="week"></div>
</div></td>



  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00"></td>
  <td class="h13 m30"></td>
  <td class="h14 m00" colspan="2"><div class="lesson color4 ui-draggable ui-draggable-handle" data-hasqtip="9"><div>
  <span class="code">CS2020</span>
  <span class="title">Data Structures and Algorithms Accelerated</span>
</div>
REC <span class="group">[2]</span>
<div class="room">COM1-0208</div>
<div class="week"></div>
</div></td>

  <td class="h15 m00"></td>
  <td class="h15 m30"></td>
  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>
</tr>
<tr>
  <td class="h08 m00"></td>
  <td class="h08 m30"></td>
  <td class="h09 m00"></td>
  <td class="h09 m30"></td>
  <td class="h10 m00"></td>
  <td class="h10 m30"></td>
  <td class="h11 m00"></td>
  <td class="h11 m30"></td>
  <td class="h12 m00"></td>
  <td class="h12 m30"></td>
  <td class="h13 m00"></td>
  <td class="h13 m30"></td>
  <td class="h14 m00"></td>
  <td class="h14 m30"></td>
  <td class="h15 m00"></td>
  <td class="h15 m30"></td>
  <td class="h16 m00"></td>
  <td class="h16 m30"></td>
  <td class="h17 m00"></td>
  <td class="h17 m30"></td>

</tr>
</tbody>

</table></div>

<?php
include("view/common/footer.php");
?>
