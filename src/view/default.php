<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/user.inc");

session_start();
if (!is_logged_in()) {
    header('Location: '.'/view/blog/login.php?status=2&redir=/view/blog/user_profile.php');
}

$user = $_SESSION['user'];
$title =  'TITLE';
$head_extra = <<<EOD
EOD;

include("view/common/head.php");
include("view/common/header.php");
require_once("view/common/elements.php");


row(<<< EOD

EOD
);

include("view/common/footer.php");
