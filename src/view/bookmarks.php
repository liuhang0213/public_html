<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = "Liu Hang - Links";
$head_extra = '';
include("view/common/head.php");
include("view/common/header.php");
?>

<div class="row">
 <div class="filler col-xs-0 col-md-2 col-lg-3"></div>
 <div class="maincontent col-xs-12 col-md-8 col-lg-6">
  <div class="title"><h2>Links</h2></div>
<div class="main-text col-xs-12">
  <p>
<a href="timetable.php">My Timetable </a><br/>
<a href="http://www.dhspriory.org/kenny">Philosophical texts, compiled by Fr. Joseph Kenny, O.P. </a><br/>
<a href="https://cgcookie.com/lesson/interface-and-navigation/">Blender Tutorials</a><br/>
<a href="http://www.northernsounds.com/forum/forumdisplay.php/77-Principles-of-Orchestration-Online">Principles of Orchestration Online</a><br />
<del><a href="https://web.archive.org/web/20120330213636/http://www2.nau.edu/~tas3/mus303/intro.html#top">Mus 303: Form and Analysis</a></del><br />
<a href="http://blakemasters.com/peter-thiels-cs183-startup#_=_">CS183: Startup—Stanford, Spring 2012</a><br/>
<a href="http://www.ee.ryerson.ca/%7Eelf/hack/recovery.html">UNIX recovery legend</a><br/>
<a href="http://vim-adventures.com/">Vim adventures</a><br/>
<a href="https://www.youtube.com/playlist?list=PLGkLBW5PxiyTBay_QqkZUqtIwq2kd4haZ">Chris Tondreau</a><br/>
</p>
</div>
</div>
</div>

<?php
include("view/common/footer.php");
?>
