<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );
require_once("helpers/utils.php");

// User class
class User {

    private $user_id;
    private $username;
    private $passwd;
    public $name;
    public $email;
    public $bio;

    function __construct($type, $value) {

        $conn = new_db_conn();

        if ($type == "username") {
            $user = $conn->query("SELECT * FROM users WHERE username = '$value';")
            ->fetch(PDO::FETCH_ASSOC);
        } else if ($type == "user_id") {
            $user = $conn->query("SELECT * FROM users WHERE user_id = $value;")
            ->fetch(PDO::FETCH_ASSOC);
        }

        $this->user_id = $user['user_id'];
        $this->username = $user['username'];
        $this->passwd = $user['passwd'];
        $this->name = $user['name'];
        $this->email = $user['email'];
        $this->bio = $user['bio'];

        $conn = NULL;
    }

    function get_user_id() {
        return $this->user_id;
    }

    function verify_login($post_passwd) {
        return password_verify($post_passwd, $this->passwd);
    }
}
