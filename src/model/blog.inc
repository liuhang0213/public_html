<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );
require_once("helpers/utils.php");

// Blog class, mainly for interacting with TABLE articles
class Blog {

    private $selected_posts;
    private $selected_user_id;
    private $selected_tag_id;


    function __construct() {
        $this->retrieve_all_articles();
    }

    function count_selected_posts() {
        return sizeof($this->selected_posts);
    }

    function new_article($title, $text, $user_id, $tags_array, $publish) {
        $conn = new_db_conn();
        $conn->query(<<< EOD
            INSERT INTO articles (title, user_id, text, published)
                VALUES ("$title", $user_id, "$text", $publish);
EOD
        );

        $article_id = $conn->lastInsertId();

        foreach ($tags_array as $tag_name) {
            $this->create_tag($tag_name);
            $tag_id = $this->get_tag_id($tag_name);
            $this->add_article_tag($article_id, $tag_id);
        }

        $doc = new DOMDocument();
        $doc->loadHTML(htmlspecialchars_decode($text));
        $img = $doc->getElementsByTagName('img');
        var_dump($img);
        if ($img->length != 0) {
            $src = $img->item(0)->getAttribute('src');
            $conn->query(<<< EOD
                UPDATE articles SET img = "$src" WHERE article_id = $article_id;
EOD
            );
        }
        $conn = NULL;
    }

    function get_article_by_id($id) {
        $conn = new_db_conn();
        $row = $conn->query("
            SELECT * FROM articles WHERE article_id = ".$id.";
            ")->fetch(PDO::FETCH_ASSOC);
        return $this->convert_db_row_to_array($row);
    }

    function get_previous_article($id) {
        $conn = new_db_conn();
        $date = $this->get_article_by_id($id)['date'];
        $row = $conn->query(<<< EOD
            SELECT * FROM articles WHERE (date < '$date') AND published = 1 ORDER BY date DESC;
EOD
        )->fetch(PDO::FETCH_ASSOC);
        return $this->convert_db_row_to_array($row);
    }

    function get_next_article($id) {
        $conn = new_db_conn();
        $date = $this->get_article_by_id($id)['date'];
        $row = $conn->query(<<< EOD
            SELECT * FROM articles WHERE (date > '$date') AND published = 1 ORDER BY date ASC;
EOD
        )->fetch(PDO::FETCH_ASSOC);
        return $this->convert_db_row_to_array($row);
    }

    function post_loader($a, $b) {
        // Return an array, containing the first $b selected posts starting from $a
        return array_slice($this->selected_posts, $a, $b);
    }

    function retrieve_all_articles() {
        // Store all articles in $this->selected_posts
        $conn = new_db_conn();
        $articles = $conn->query("
            SELECT * FROM articles ORDER BY date DESC;
            ");
        $fetched_array = $articles->fetchAll();
        $conn = NULL;
        foreach ($fetched_array as &$row) {
            $row = $this->convert_db_row_to_array($row);
        }

        $this->selected_posts = $fetched_array;
        return;
    }

    function filter_articles($pred) {
        $new_array = array();
        foreach ($this->selected_posts as $key => $post) {
            if ($pred($post)) {
                $new_array[] = $post;
            }
        }
        $this->selected_posts = $new_array;
    }

    function filter_articles_by_user_id($user_id) {
        $this->selected_user_id = $user_id;
        return $this->filter_articles(
            function($article) {
                return ($article['user_id'] === $this->selected_user_id);
        });
    }

    function filter_articles_by_tag_id($tag_id) {
        $this->selected_tag_id = $tag_id;
        return $this->filter_articles(
            function($article) {
                return $this->is_article_tagged($article['article_id'],
                    $this->selected_tag_id);
        });
    }

    function filter_published_articles() {
        return $this->filter_articles(
            function ($article) {
                return $article['published'];
            }
        );
    }

    function filter_articles_by_date($month, $year) {
        $conn = new_db_conn();
        if ($month == 0) {
            $start_date = date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, $year));
            $end_date = date("Y-m-d H:i:s", mktime(0, 0, 0, 1, 1, $year + 1));
        } else {
            $start_date = date("Y-m-d H:i:s", mktime(0, 0, 0, $month, 1, $year));
            $end_date = date("Y-m-d H:i:s", mktime(0, 0, 0, $month + 1, 1, $year));
        }

        $fetched_array = $conn->query(<<< EOD
            SELECT * FROM articles WHERE date BETWEEN '$start_date' AND '$end_date'
            ORDER BY date DESC;
EOD
        )->fetchAll();
        $conn = NULL;
        foreach ($fetched_array as &$row) {
            $row = $this->convert_db_row_to_array($row);
        }
        $this->selected_posts = $fetched_array;
        return;
    }


    function search_articles($keyword) {
        $conn = new_db_conn();
        $fetched_array = $conn->query(<<<EOD
            SELECT * FROM articles WHERE MATCH (title, text)
            AGAINST ("$keyword" IN NATURAL LANGUAGE MODE) ORDER BY DATE DESC;
EOD
            )->fetchAll();
        $conn = NULL;
        foreach ($fetched_array as &$row) {
            $row = $this->convert_db_row_to_array($row);
        }

        $this->selected_posts = $fetched_array;
        return;
    }

    function update_article($id, $title, $text, $tags_array, $published) {
        $conn = new_db_conn();
        $conn->query(<<< EOD
            UPDATE articles SET title="$title", text="$text", published=$published, img=NULL WHERE article_id=$id;
EOD
        );
        $this->delete_article_tag_records($id);

        foreach ($tags_array as $tag_name) {
            $this->create_tag($tag_name);
            $tag_id = $this->get_tag_id($tag_name);
            $this->add_article_tag($id, $tag_id);
        }

        $doc = new DOMDocument();
        $doc->loadHTML(htmlspecialchars_decode($text));
        $img = $doc->getElementsByTagName('img');
        if ($img->length != 0) {
            $src = $img->item(0)->getAttribute('src');
            $conn->query(<<< EOD
                UPDATE articles SET img = "$src" WHERE article_id = $id;
EOD
            );
        }
        $conn = NULL;
    }

    function delete_article($id) {
        // Tags will be deleted automatically by cascading deletes
        $conn = new_db_conn();
        $conn->query(<<< EOD
            DELETE FROM articles WHERE article_id=$id;
EOD
    );
        $conn = NULL;
    }

    function convert_db_row_to_array($row) {
        // Given a row fetched from db, get & append extra information to the array
        // including author's name, id and permalink.
        $conn = new_db_conn();
        if (!isset($row['user_id'])) {
            // Row is empty
            return false;
        }

        $author = $conn->query("
            SELECT * FROM users WHERE user_id = ".$row['user_id'].";
        ")->fetch(PDO::FETCH_ASSOC);
        $author_name = $author['name'];
        $author_id = $author['user_id'];
        $link = "/view/blog/view_article?id=".$row['article_id'];
        $conn = NULL;
        $row['author'] = $author_name;
        $row['author_id'] = $author_id;
        $row['link'] = $link;
        return $row;
    }

    // Tagging

    function create_tag($tag_name) {
        // Insert a new row to TABLE tags
        $conn = new_db_conn();
        $conn->query(<<< EOD
            INSERT IGNORE INTO tags (tag_name) VALUE ("$tag_name");
EOD
        );
        $conn = NULL;
    }

    function add_article_tag($article_id, $tag_id) {
        // Insert a new row to TABLE article_tag
        $conn = new_db_conn();
        $conn->query(<<< EOD
            INSERT INTO article_tag (article_id, tag_id)
            VALUES ($article_id, $tag_id);
EOD
        );
        $conn = NULL;
    }

    function get_tag_id($tag_name) {
        $conn = new_db_conn();
        $id = $conn->query(<<< EOD
            SELECT * FROM tags WHERE tag_name = "$tag_name";
EOD
            )->fetch(PDO::FETCH_ASSOC)['tag_id'];
        $conn = NULL;
        return $id;
    }

    function get_tag_name($tag_id) {
        $conn = new_db_conn();
        $id = $conn->query(<<< EOD
            SELECT * FROM tags WHERE tag_id = $tag_id;
EOD
            )->fetch(PDO::FETCH_ASSOC)['tag_name'];
        $conn = NULL;
        return $id;
    }

    function get_tag_counts() {
        // Counting all tags, for generating the tag list on sidebar
        // but excluding draft articles
        $conn = new_db_conn();
        $fetched_array = $conn->query(<<< EOD
        SELECT tag_id, count(tag_id)
        FROM article_tag
        INNER JOIN articles
        ON article_tag.article_id = articles.article_id
        WHERE articles.published = 1
        GROUP by tag_id
        ORDER BY count(tag_id) DESC;
EOD
    )->fetchAll();
        foreach ($fetched_array as &$row) {
            $row['tag_name'] = $this->get_tag_name($row['tag_id']);
        }
         return $fetched_array;
    }

    function is_article_tagged($article_id, $tag_id) {
        $conn = new_db_conn();
        $re = $conn->query("
            SELECT 1 FROM article_tag WHERE (article_id, tag_id) = ($article_id, $tag_id);
        ")->fetch(PDO::FETCH_ASSOC);
        return $re;
    }

    function get_tags_by_article_id($article_id) {
        $conn = new_db_conn();
        $rows = $conn->query(<<< EOD
            SELECT * FROM article_tag WHERE article_id = $article_id;
EOD
        )->fetchAll();
        $tag_name_array = array();
        foreach ($rows as $row) {
            $tag_name_array[] = $this->get_tag_name($row['tag_id']);
        }
        return $tag_name_array;
    }

    function count_tagged_articles($tag_id) {
        // Include unpublished articles
        $conn = new_db_conn();
        $rows = $conn->query(<<< EOD
            SELECT * FROM article_tag WHERE tag_id = $tag_id
EOD
        )->fetchAll();
        return sizeof($rows);
    }

    function delete_article_tag_records($article_id) {
        $conn = new_db_conn();
        $conn->query(<<< EOD
            DELETE FROM article_tag WHERE article_id = $article_id;
EOD
        );
        $conn = NULL;
    }

    function delete_tag($tag_id) {
        if ($this->count_tagged_articles($tag_id) != 0) {
            // Prevent deleting tags by accident
            return 0;
        }

        $conn = new_db_conn();
        $conn->query(<<< EOD
            DELETE FROM tags WHERE tag_id = $tag_id;
EOD
        );
        $conn = NULL;
    }

    function delete_unused_tags() {
        $conn = new_db_conn();
        $tags = $conn->query(<<< EOD
            SELECT * FROM tags;
EOD
        )->fetchAll();

        $conn = NULL;
        foreach ($tags as $tag) {
            $this->delete_tag($tag['tag_id']);
        }
    }
}
