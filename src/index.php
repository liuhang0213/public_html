<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

$title = "Liu Hang - Home";
$head_extra = <<<EOD
<link rel="stylesheet" href="/static/css/index.css">
EOD;

include("view/common/head.php");
require_once("view/common/elements.php");
?>

<!-- Banner & Navbar -->

<body>
<a href="#top"></a>
 <div class="row">
  <div class="header-container col-xs-12">
    <h1 class="header-title">Liu Hang</h1>
   <h3 class="header-title">-My Personal Page-</h3>

  </div>
 </div>

 <input type="checkbox" class="navbutton-checkbox" id="nav" checked/>
 <div class="row">
  <div class="navbar-nav col-xs-12" id="navbar">
   <table class="nav-center">
    <tr>
     <td><a href="#about">About</a></td>
     <td><a href="#projects">Projects</a></td>
     <td><a href="#blog">Blog</a></td>
     <td><a href="#links">Links</a></td>
    </tr>
  </table>
 </div>
 <div class="navbutton">
   <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
 </div>
</div>

<?php
row(<<< EOD
   <div class="title"><a name="about"></a>
     <a href="/view/about.php"><h2 class="title-link">About me <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2></a>
   </div>
   <div class="banner" id="about-me-pic">
   </div>
   <div class="main-text col-xs-12">
     <p>If this text shows up, everything is working correctly.  <a href="/view/about.php">Read more about me.</a></p>
   </div>
EOD
);

row(<<< EOD
   <div class="title"><a name="projects"></a>
     <a href="/view/projects/project_index.php"><h2 class="title-link">My projects <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2></a>
   </div>
   <div class="banner" id="projects-pic">
   </div>
   <div class="main-text col-xs-12">
     <p>Things I did in my free time. <a href="/view/projects/project_index.php">Read more about my projects.</a></p>
   </div>
EOD
);

row(<<< EOD
   <div class="title"><a name="blog"></a>
     <a href="/view/blog/articles_index.php"><h2 class="title-link">My Blog <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2></a>
   </div>
   <div class="banner" id="blog-pic">
   </div>
   <div class="main-text col-xs-12">
     <p>This is my <a href="/view/blog/articles_index.php">blog</a>.
   </div>
EOD
);

row(<<< EOD
   <div class="title"><a name="links"></a>
     <a href="/view/bookmarks.php"><h2 class="title-link">External links <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2></a>
   </div>
   <div class="banner" id="blog-pic">
   </div>
   <div class="main-text col-xs-12">
     <p><a href="/view/bookmarks.php">Interesting, useful things I found online.</a></p>
   </div>
EOD
);

row(<<< EOD
      <div class="banner" id="soundcloud">
        <iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/257987580&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
      </div>
EOD
);

include("view/common/footer.php");
?>
