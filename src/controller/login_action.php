<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );
require("model/user.inc");

$username = $_POST['username'];
$post_passwd = $_POST['passwd'];

$user = new User("username", $username);

if ($user->verify_login($post_passwd)) {
    session_start();
    $_SESSION['user'] = $user;
    if (isset($_SESSION['redirect_from'])) {
        header('Location: '.$_SESSION['redirect_from']);
    } else {
        header('Location: '.'/view/blog/user_dashboard.php');
    }

} else {
    header('Location: '.'/view/blog/login.php?status=1&username='.$username);
}

?>
