<?php
// For both creating new articles and updating existing ones.
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/blog.inc");
require_once("model/user.inc");

session_start();
if (!is_logged_in()) {
    header('Location: '.'/view/blog/login.php?status=2&redir=/view/blog/user_profile.php');
    exit();
} else {
    $user = $_SESSION['user'];
}

$blog = new Blog();

if ($_GET['new']) {
    $blog->new_article(htmlspecialchars($_POST['title']),
        htmlspecialchars($_POST['text']),
        $user->get_user_id(),
        json_decode($_POST['tags']),
        $_POST['publish']
    );
} else {
    $blog->update_article($_SESSION['article_id'],
        htmlspecialchars($_POST['title']),
        htmlspecialchars($_POST['text']),
        json_decode($_POST['tags']),
        $_POST['publish']
    );
}

header('Location: '.'/view/blog/user_dashboard.php');
