<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );

require_once("helpers/utils.php");

require_once("model/blog.inc");
require_once("model/user.inc");

session_start();
if (!is_logged_in()) {
    header('Location: /view/blog/login.php?status=2&redir=/view/blog/user_article_list');
    exit();
} else {
    $user = $_SESSION['user'];
}

$blog = new Blog();

if (isset($_GET['article_id'])) {
    $article = $blog->get_article_by_id($_GET['article_id']);
} else {
    header('Location: /view/404');
    exit();
}

if ($user->get_user_id() == $article['user_id']) {
    // Check whether the user is same as author.
    $blog->delete_article($article['article_id']);
} else {
    header('Location: /view/404');
    exit();
}

header('Location: '.'/view/blog/user_article_list');
