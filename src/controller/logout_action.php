<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] );
session_start();
session_unset();
session_destroy();
header('Location: '.'/view/blog/articles_index.php');
exit;
